#+TITLE: Meeting with IIT Bombay reg. experiment hosting
#+AUTHOR: VLEAD
#+DATE: [2019-08-27 Tue]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
This document holds the minutes from the meeting held on
[2019-08-27 Tue] with IIT Bombay.

* Attendees
  1. Pushpdeep
  2. Priya
  3. Thirumal

* Agenda
Gain clarity on the step 3 of the experiment development
process adhered to by all the institutes in phase 3.

* Discussion
1. Thirumal demonstrated the [[http://exp-iiith.vlabs.ac.in/bs/exp.html#Bubble%2520Sort%2520Experiment][bubble sort]] experiment built as
   part of phase 3.
2. It was agreed that the experiment authoring process
   adhered at VLEAD defines semantics.  An experiment
   consists of Learning Units, and each learning unit is
   made up of tasks and each task is made up of artifacts.
3. The authoring of the content and the development of
   artifacts are separated from the final rendering of the
   experiment.
4. This is made possible because of existence of a
   specification at every stage of experiment building.
5. Pushpdeep felt through this process, an alignment with
   pedagogy could be made possible.
6. Pushpdeep explained the process of experiment development
   and hosting process followed at IIT Bombay.  This process
   also adheres to a specification, but this spec defines
   the structure of an experiment in terms of sections -
   Introduction, Simulation, Referencec etc.  Each section
   is a markdown file, while the simulation is an artifact
   written in =js=.
7. Currently, all the experiments built by IIT Bombay are
   hosted and tied to gitlab's CI.  The plan is to separate
   the simulations to a different server and build a process
   to link the content of an experiment with the simulation.
8. Static web site builder software like Jekyll is used for
   the final rendering of the experiment because each
   experiment fits the same structure with a corresponding
   content file - either a md or js.
9. It was not decided in this meeting if IIT Bombay will
   switch to VLEAD process of experiment building or if it
   will accommodate VLEAD's process. 


* Action
1. It was decided to share the design and documents of both
   the processes.
2. Thirumal will share the experiment runtime environment
   (ere) and help Pushpdeep build one of the experiments on
   his laptop. 
3. Similarly, Pushpdeep will share the sources of experiment
   building process at IIT Bombay.
