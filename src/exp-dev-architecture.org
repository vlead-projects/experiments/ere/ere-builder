#+TITLE: Experiment Development Work Flow and Architecture
#+AUTHOR:VLEAD
#+DATE: [2019-04-25 Thu]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  This document provides an overview of the experiment
  development work flow and architecture.

  We are developing a platform for experiment development to
  keep in pace with growing needs of students. The
  experiments across domains are conceptualized, aligned and
  built using latest technologies. They are continuously
  re-factored and made FOSS ready.  Currently we are
  developing data structures experiments.  The experiment
  platform (ere) separates content creation and
  presentation. The content of an experiment is authored in
  HTML5 (standard HTML) by the author. The platform serves
  the content along with predefined presentation creating a
  dynamic and enriched learning environment. This
  facilitates content as a service.

* Work Flow
  The construction of an experiment flows through multiple
  stages to get to a certain fixed point.

  #+CAPTION: Roles and Contributions 
  #+NAME: Roles and Contributions
  #+ATTR_HTML: :width 100% :height 80% :display block :margin auto  
  [[./images/exp-dev-roles.png]]

  + =Instructor= ::
    The instructor provides the content of an experiment
    authored in HTML5 (standard html) using the [[https://gitlab.com/vlead-projects/experiments/ere/specs/blob/master/src/content/std-html.org][specs]] (grammar) 
    that conforms to a style of pedagogy.

  + =Instructional Designer (ID)= ::
    An ID structures the content provided by instructor to
    improve learning experience for a given target
    audience. During this process, new content is also
    added.
  
  + =UI/UX Designer= ::
    The UI/UX designers maps the content provided by the ID
    to a computer screen which is the medium of delivery.
  
  + =Application Programmer= :: 
    The application programmer builds the artefacts specified
    by an instructor and instructional designer as per the 
    designs given by the UI/UX designer.
    
* Architecture  
  #+CAPTION: Experiment Development Architecture
  #+NAME: Experiment Development Architecture
  #+ATTR_HTML: :width 100% :height 80% 
  [[./images/exp-dev-architecture.png]]
  
  The above picture provides an overview of the experiment
  development architecture and gives you a clear picture, on
  how the experiment is built in various stages, how each
  stage is interlinked and how the experiment is realized as
  an application with the [[][ere]].

  Please view [[][this]] section to know about the ere
  components.

* How the published ere is used to render an experiment
  At development time, the author puts content =exp.html=
  under =src/=.  The build process is driven by the
  makefile, which downloads the published sources and puts
  under =build/=.

  It is the makefile��s responsibility to
  1. Copy =exp.html= file to =build/=
  2. Pull the latest or specified release of the =ere= into
     =build/=
  3. The makefile may create other intermediate directories
  
